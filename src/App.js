import logo from './logo.svg';
import './App.css';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import React from 'react';
// import { Query } from "react-apollo";
import gql from "graphql-tag";

function App() {

  function renderAPI()
  {
    console.log("then api");
    const client = new ApolloClient({
      uri: "https://countries-274616.ew.r.appspot.com/"
    });
    // console.log("client", client);
    client.query({
      query: gql`
      query {
        Country {
          name,
          area,
          capital,
          population
        }
      }
      `,
    })
      .then(data => console.log("data retrieved", data))
      .catch(error => console.error(error));
  }


  console.log("first app");
  return (

    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button onClick={renderAPI} className="btn"> Call API !!</button>
      </header>
    </div>
  );
}

export default App;


{/* <ApolloProvider client={client}>
        <div>
          <h2>My first Apollo app</h2>
        </div>
      </ApolloProvider> */}

// import React, { Component } from 'react';
// import LinkList from './components/linkList';
// class App extends Component {
//   render() {
//     return <LinkList />;
//   }
// }

// export default App;
